<div align="center">
  <h1>Adapter</h1>
</div>

<div align="center">
  <img src="adapter_icon.png" alt="drawing" width="250"/>
</div>

## Table of Contents

1. **[What is it?](#what-is-it)**
    1. [Real-World Analogy](#real-world-analogy)
    2. [Participants](#participants)
    3. [Collaborations](#collaborations)
2. **[When do you use it?](#when-do-you-use-it)**
    1. [Motivation](#motivation)
    2. [Known Uses](#known-uses)
    3. [Categorization](#categorization)
    4. [Aspects that can vary](#aspects-that-can-vary)
    5. [Solution to causes of redesign](#solution-to-causes-of-redesign)
    6. [Consequences](#consequences)
    7. [Relations with Other Patterns](#relations-with-other-patterns)
3. **[How do you apply it?](#how-do-you-apply-it)**
    1. [Structure](#structure)
    2. [Variations](#variations)
    3. [Implementation](#implementation)
4. **[Sources](#sources)**

<br>
<br>

## What is it?

> :large_blue_diamond: **Adapter is a structural pattern that allows objects with incompatible interfaces to
collaborate.**

### Real-World Analogy

_A power plug adapter._

A power plug adapter (analogy for adapter class) enables you to plug your european laptop charger (service) into a US
power socket (incompatible interface).

### Participants

- :bust_in_silhouette: **Target**
    - Defines an interface to:
        - `clientCompatibleMethodA`: method signature expected by client.
- :man: **Adaptee**
    - Provides implementation for:
        - `incompatibleMethodA`: method which doesn't conform to client's expectations.
- :man: **Adapter**
    - Provides an implementation for:
        - `clientCompatibleMethodA`: translates method in target interface to method of the adaptee.

### Collaborations

Clients call operations on an **Adapter** instance. In turn, the **Adapter** calls **Adaptee** operations that carry out
the request.


<br>
<br>

## When do you use it?

> :large_blue_diamond: **When you want to unify the interface of multiple (vendor) classes. Or various clients
require one class to have different interfaces.**

### Motivation

- How can a class cooperate with unrelated or unforeseen classes that expect different interfaces?
- How can we standardize the interfaces of different vendor classes that we cannot alter?

### Known Uses

- Third party Library or API Adaptation:
    - Adapting an existing library or API to fit the needs of your application without changing its
      core functionality.
- Legacy Integration:
    - When you need to incorporate legacy systems or components that have a different interface into a new system
      without modifying their code.
- GUI components
    - Adapting the domain specific interface of classes (`get_height` or `get_price`) to what is required for
      a GUI component (`get_value`).
- Database access:
    - Standardize interface to interact with different databases.
- Protocol Conversion:
    - For example, converting data from one protocol (like XML) to another (like JSON).
- Testing:
    - Creating mock or fake objects that adapt to the interface of the real objects to facilitate testing.

### Categorization

Purpose:  **Structural**  
Scope:    **Class & Object**   
Mechanisms: **Polymorphism, Composition**

Structural patterns are concerned with how classes and objects are composed to form larger structures.
Structural object patterns describe ways to compose objects to realize new functionality.

### Aspects that can vary

- Interface to an object.

### Solution to causes of redesign

- Inability to alter classes conveniently.
    - e.g: classes from a commercial class library
    - or the change would require modifying many existing subclasses

### Consequences

| Advantages                                                                                                                                                                                                          | Disadvantages                                                                                                                                                                                                                                            |
|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| :heavy_check_mark: **Adapt multiple Adaptees.** <br> A single Adapter can work with many Adaptees. That is, the Adaptee itself and all of its subclasses (if any).                                                  | :x: **Harder to override Adaptee behavior.** <br> Will require subclassing Adaptee and making Adapter refer to the subclass rather than the Adaptee itself                                                                                               |
| :heavy_check_mark: **Eliminate assumption that clients see the same interface.** <br> Interface adaptation lets us incorporate our class into existing systems that might expect different interfaces to the class. | :x: **Not transparent to all clients.** <br>  An adapted object no longer conforms to the Adaptee interface, so it can't be used as-is wherever an Adaptee object can.                                                                                   |
|                                                                                                                                                                                                                     | :x: **Increased Indirection.** <br>  The Adapter pattern adds an extra level of indirection between the Client code and the Adaptee. This indirection can make the code harder to follow and debug, especially if there are multiple layers of adapters. |

### Relations with Other Patterns

_Distinction from other patterns:_

- Decorator enhances another object without changing its interface.
    - A decorator is thus more transparent to the application than an adapter is. As a consequence, Decorator supports
      recursive composition, which isn't possible with pure adapters.
- Proxy defines a representative or surrogate for another object and does not change its interface.
    - Adapter changes the interface of the objects it adapts, while the Proxy implements the same interface
- Facade also modifies the interface, but with the intent to provide a simplified interface to a subsystem
    - The intent of the Adapter Pattern is to alter an interface so that it matches one a client is expecting
- At first sight, the Bridge pattern looks a lot like the Adapter pattern in that a class is used to convert one kind of
  interface to another.
    - The Adapter pattern is geared toward making unrelated classes work together. It is usually applied to systems
      after they're designed.
    - Bridge, on the other hand, is used up-front in a design to let abstractions and implementations vary
      independently.

_Combination with other patterns:_

- Factories are often employed to enforce the use of adapters.
    - They can wrap the subject with an adapter before returning it from the factory. The client never
      knows or cares that it’s using the adapter instead of the real thing

<br>
<br>

## How do you apply it?

> :large_blue_diamond: **An adapter class with methods that redirect translated calls to a stored object.**

### Structure

```mermaid
classDiagram
    class Target {
        <<interface>>
        + clientCompatibleMethodA()*
    }

    class Adapter {
        - adaptee: Adaptee
        + clientCompatibleMethodA()
    }

    class Adaptee {
        + incompatibleMethodA()
    }

    Target <|.. Adapter: implements
    Adapter *-- Adaptee: composes
```

### Variations

_Class vs object adapter:_

- **Class adapter**: Adapts Adaptee to Target by committing to a concrete Adaptee class.
    - :heavy_check_mark: Allows overriding some of Adaptee's behavior, since Adapter is a subclass of Adaptee.
    - :heavy_check_mark: Introduces only one object, and no additional indirection to get to the adaptee.
    - :x: Won't work when we want to adapt a class and all its subclasses
- **Object adapter**: Adapts to target using composition.
    - :heavy_check_mark: lets a single Adapter work with Adaptee and all of its subclasses.
    - :x: harder to override Adaptee behavior.

_Other variations:_

- **Two-way adapter**: Implements both target and adaptee interface.
    - :heavy_check_mark: Provides transparency: adapter can be used as-is wherever Adaptee object can.
    - :heavy_check_mark: Allows two clients to each use an object in different ways.
    - :x: More clients coupled to the adapter interface.
- **Pluggable adapter**: Adapter with an overloaded constructor, accepting either a target or adaptee object, and
  changing the methods called based on the type of object it received.
    - :heavy_check_mark: increase the chances of adaptation by other developers.
    - :x: Not all languages support overloaded constructors.
- **Fat adapter**: Adapter that holds two or more adaptees to implement the target interface.
    - :heavy_check_mark: sometimes necessary.
    - :x: Couples adapter to multiple classes.

### Implementation

In the example we apply the adapter pattern to in a todo app where you can choose different types of databases for
storage. (code in the abstract factory pattern repository)

- [Interface (interface)]()
- [Concrete Oubject]()

The _"client"_ is a ... program:

- [Client]()

The unit tests exercise the public interface of ... :

- [Concrete Object test]()

<br>
<br>

## Sources

- [Design Patterns: Elements Reusable Object Oriented](https://www.amazon.com/Design-Patterns-Elements-Reusable-Object-Oriented/dp/0201633612)
- [Refactoring Guru - Adapter](https://refactoring.guru/design-patterns/adapter)
- [Head First Design Patterns: A Brain-Friendly Guide](https://www.amazon.com/Head-First-Design-Patterns-Brain-Friendly/dp/0596007124)
- [ArjanCodes: Let's Take The Adapter Design Pattern To The Next Level](https://youtu.be/fsB8_79zI_A?si=IQkwuqi2Iw0I_SCd)
- [pentalog: adapter-design-pattern](https://www.pentalog.com/blog/design-patterns/adapter-design-pattern/)

<br>
<br>
